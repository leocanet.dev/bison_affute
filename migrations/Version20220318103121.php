<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220318103121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actualite DROP visibilite_art');
        $this->addSql('ALTER TABLE media DROP visibilite_med');
        $this->addSql('ALTER TABLE prestations DROP visibilite_pres');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actualite ADD visibilite_art TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE media ADD visibilite_med TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE prestations ADD visibilite_pres TINYINT(1) DEFAULT NULL');
    }
}
