<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220316124848 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actualite ADD image_actu VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE presentation ADD image_pres_1 VARCHAR(255) DEFAULT NULL, ADD image_pres_2 VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actualite DROP image_actu');
        $this->addSql('ALTER TABLE presentation DROP image_pres_1, DROP image_pres_2');
    }
}
