let arrow = document.querySelector(".arrow");
arrow.addEventListener("click", () => {
  let about = document.querySelector("#section1");
  about.scrollIntoView({ behavior: "smooth", block: "start", inline: "start" });
});

let d = new Date();
let n = d.getDay();
let now = d.getHours() + "." + d.getMinutes();
let weekdays = [
  ["Sunday"], // fermé
  ["Monday", 9.0, 12.0, 14.0, 18.0],
  ["Tuesday", 9.0, 12.0, 14.0, 18.0],
  ["Wednesday", 9.0, 12.0, 14.0, 18.0],
  ["Thursday", 9.0, 12.0, 14.0, 18.0],
  ["Friday", 9.0, 12.0, 14.0, 18.0],
  ["Saturday"], //  fermé
];
let day = weekdays[n];

if ((now > day[1] && now < day[2]) || (now > day[3] && now < day[4])) {
  document.getElementById("currently_open").innerHTML = "Actuellement ouvert";
  document.getElementById("open").classList.remove("close");
} else {
  document.getElementById("currently_open").innerHTML = "Actuellement fermé";
  document.getElementById("open").classList.add("close");
  document.getElementById("currently_open").classList.add("currently_closed");
}
