<?php

namespace ContainerYKaEinp;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getActualite2Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.cKxijlj.App\Entity\Actualite' shared service.
     *
     * @return \App\Entity\Actualite
     */
    public static function do($container, $lazyLoad = true)
    {
        throw new RuntimeException('Cannot autowire service ".service_locator.cKxijlj": it references class "App\\Entity\\Actualite" but no such service exists.');
    }
}
