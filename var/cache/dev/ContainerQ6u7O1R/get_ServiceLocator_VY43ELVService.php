<?php

namespace ContainerQ6u7O1R;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_VY43ELVService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.vY43ELV' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.vY43ELV'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'presentation' => ['privates', '.errored..service_locator.vY43ELV.App\\Entity\\Presentation', NULL, 'Cannot autowire service ".service_locator.vY43ELV": it references class "App\\Entity\\Presentation" but no such service exists.'],
        ], [
            'presentation' => 'App\\Entity\\Presentation',
        ]);
    }
}
