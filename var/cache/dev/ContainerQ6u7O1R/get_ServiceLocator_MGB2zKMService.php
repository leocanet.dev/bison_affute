<?php

namespace ContainerQ6u7O1R;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_MGB2zKMService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.mGB2zKM' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.mGB2zKM'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'actualiteRepository' => ['privates', 'App\\Repository\\ActualiteRepository', 'getActualiteRepositoryService', true],
            'fileUploader' => ['privates', 'App\\Service\\FileUploader', 'getFileUploaderService', true],
        ], [
            'actualiteRepository' => 'App\\Repository\\ActualiteRepository',
            'fileUploader' => 'App\\Service\\FileUploader',
        ]);
    }
}
