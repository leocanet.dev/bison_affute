<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* coordonnees/index.html.twig */
class __TwigTemplate_3dbce573dfa97eaab57ba408c1ecfce9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "coordonnees/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "coordonnees/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "coordonnees/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tContact
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t<main class=\"bodycontact\">

\t\t<div class=\"flexh\">

\t\t\t<div class=\"columnh\">
\t\t\t\t<h2>
\t\t\t\t\tOù me trouver
\t\t\t\t</h2>
\t\t\t\t<iframe id=\"map\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.2335569898796!2d2.898306315172554!3d43.24652667913739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1b1a9101872ff%3A0xe962e680efe5f622!2s8%20Rue%20des%20Oliviers%2C%2011120%20Saint-Nazaire-d&#39;Aude!5e0!3m2!1sfr!2sfr!4v1643372465129!5m2!1sfr!2sfr\" width=\"400\" height=\"300\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
\t\t\t</div>

\t\t\t<div class=\"formcontact\">
\t\t\t\t<form id=\"contact\" action=\"../controleur/mail.php\" method=\"post\">
\t\t\t\t\t<h2>
\t\t\t\t\t\tCoordonnées
\t\t\t\t\t</h2>
\t\t\t\t\t<div class=\"coord\">
\t\t\t\t\t\t";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["coordonnees"]) || array_key_exists("coordonnees", $context) ? $context["coordonnees"] : (function () { throw new RuntimeError('Variable "coordonnees" does not exist.', 26, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["coordonnee"]) {
            // line 27
            echo "\t\t\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 28
                echo "\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["coordonnee"], "id", [], "any", false, false, false, 28)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
            }
            // line 32
            echo "\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["coordonnee"], "adresse", [], "any", false, false, false, 33), "html", null, true);
            echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["coordonnee"], "telephone", [], "any", false, false, false, 36), "html", null, true);
            echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<a href=\"mailto:";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["coordonnee"], "mail", [], "any", false, false, false, 38), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["coordonnee"], "mail", [], "any", false, false, false, 39), "html", null, true);
            echo "
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["coordonnee"], "horaires", [], "any", false, false, false, 42), "html", null, true);
            echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 45
            echo "\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\tAucunes coordonnées..
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['coordonnee'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "\t\t\t\t\t</div>

\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t</main>


";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "coordonnees/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 49,  158 => 45,  150 => 42,  144 => 39,  140 => 38,  135 => 36,  129 => 33,  126 => 32,  118 => 28,  115 => 27,  110 => 26,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tContact
{% endblock %}

{% block body %}

\t<main class=\"bodycontact\">

\t\t<div class=\"flexh\">

\t\t\t<div class=\"columnh\">
\t\t\t\t<h2>
\t\t\t\t\tOù me trouver
\t\t\t\t</h2>
\t\t\t\t<iframe id=\"map\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.2335569898796!2d2.898306315172554!3d43.24652667913739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1b1a9101872ff%3A0xe962e680efe5f622!2s8%20Rue%20des%20Oliviers%2C%2011120%20Saint-Nazaire-d&#39;Aude!5e0!3m2!1sfr!2sfr!4v1643372465129!5m2!1sfr!2sfr\" width=\"400\" height=\"300\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
\t\t\t</div>

\t\t\t<div class=\"formcontact\">
\t\t\t\t<form id=\"contact\" action=\"../controleur/mail.php\" method=\"post\">
\t\t\t\t\t<h2>
\t\t\t\t\t\tCoordonnées
\t\t\t\t\t</h2>
\t\t\t\t\t<div class=\"coord\">
\t\t\t\t\t\t{% for coordonnee in coordonnees %}
\t\t\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t\t\t\t<a href=\"{{ path('app_coordonnees_edit', {'id': coordonnee.id}) }}\">
\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t{{ coordonnee.adresse }}
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t{{ coordonnee.telephone }}
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<a href=\"mailto:{{ coordonnee.mail }}\">
\t\t\t\t\t\t\t\t{{ coordonnee.mail }}
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t{{ coordonnee.horaires }}
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\tAucunes coordonnées..
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</div>

\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t</main>


{% endblock %}
", "coordonnees/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/coordonnees/index.html.twig");
    }
}
