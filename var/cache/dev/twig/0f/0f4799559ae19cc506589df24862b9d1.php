<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* actualite/index.html.twig */
class __TwigTemplate_c84fe9e31e73f78bd97e338db12b328b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "actualite/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "actualite/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "actualite/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tActualités

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "\t<style>
\t\t.pagination {
\t\t\tdisplay: inline-block;
\t\t}

\t\t.pagination a {
\t\t\tcolor: #F9F617;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #000000;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.current {
\t\t\tcolor: #000000;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #F9F617;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.pagination a.active {
\t\t\tbackground-color: #4CAF50;
\t\t\tcolor: white;
\t\t\tborder: 1px solid #4CAF50;
\t\t}

\t\t.pagination a:hover:not(.active) {
\t\t\tbackground-color: #F9F617;
\t\t\tcolor: black;
\t\t}
\t</style>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 45
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 46
        echo "
\t<main class=\"mainActu\">
\t\t<div class=\"news\">
\t\t\t<p class=\"titreActu\">
\t\t\t\tDernières actualités
\t\t\t</p>
\t\t</div>

\t\t";
        // line 54
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 55
            echo "\t\t\t<a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_new");
            echo "\">
\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t</a>
\t\t";
        }
        // line 59
        echo "
\t\t";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["actualites"]) || array_key_exists("actualites", $context) ? $context["actualites"] : (function () { throw new RuntimeError('Variable "actualites" does not exist.', 60, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["actualite"]) {
            // line 61
            echo "\t\t\t<div class=\"containerNewArticle\">
\t\t\t\t<div class=\"border\"></div>
\t\t\t\t";
            // line 63
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 64
                echo "\t\t\t\t<div class=\"input-admin\">
                    <a title=\"Supprimer\" href=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["actualite"], "id", [], "any", false, false, false, 65)]), "html", null, true);
                echo "\"><i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i></a>
\t\t\t\t\t<a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["actualite"], "id", [], "any", false, false, false, 66)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t";
            }
            // line 71
            echo "\t\t\t\t<p class=\"titreActu2\">
\t\t\t\t\t";
            // line 72
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "titreArt", [], "any", false, false, false, 72), "html", null, true);
            echo "
\t\t\t\t</p>
\t\t\t\t<p class=\"dateActu\">
\t\t\t\t\t<small>Publié le :
\t\t\t\t\t\t";
            // line 76
            ((twig_get_attribute($this->env, $this->source, $context["actualite"], "dateArt", [], "any", false, false, false, 76)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "dateArt", [], "any", false, false, false, 76), "Y-m-d"), "html", null, true))) : (print ("")));
            echo "
\t\t\t\t\t</small>
\t\t\t\t</p>
\t\t\t\t<img class=\"imgActu\" src=\"/uploads/";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "imageActu", [], "any", false, false, false, 79), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "titreArt", [], "any", false, false, false, 79), "html", null, true);
            echo "\">
\t\t\t\t<p class=\"textActu\">
\t\t\t\t\t";
            // line 81
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "descriptionArt", [], "any", false, false, false, 81), "html", null, true);
            echo "
\t\t\t\t</p>
\t\t\t</div>
\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 85
            echo "\t\t\t<p>
\t\t\t\tAucune actualité, veuillez en créer de nouvelles
\t\t\t</p>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actualite'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "
\t\t<div class=\"navigation\">
\t\t\t";
        // line 91
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["actualites"]) || array_key_exists("actualites", $context) ? $context["actualites"] : (function () { throw new RuntimeError('Variable "actualites" does not exist.', 91, $this->source); })()));
        echo "
\t\t</div>

\t</main>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "actualite/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 91,  236 => 89,  227 => 85,  218 => 81,  211 => 79,  205 => 76,  198 => 72,  195 => 71,  187 => 66,  183 => 65,  180 => 64,  178 => 63,  174 => 61,  169 => 60,  166 => 59,  158 => 55,  156 => 54,  146 => 46,  136 => 45,  92 => 9,  82 => 8,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tActualités

{% endblock %}

{% block stylesheets %}
\t<style>
\t\t.pagination {
\t\t\tdisplay: inline-block;
\t\t}

\t\t.pagination a {
\t\t\tcolor: #F9F617;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #000000;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.current {
\t\t\tcolor: #000000;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #F9F617;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.pagination a.active {
\t\t\tbackground-color: #4CAF50;
\t\t\tcolor: white;
\t\t\tborder: 1px solid #4CAF50;
\t\t}

\t\t.pagination a:hover:not(.active) {
\t\t\tbackground-color: #F9F617;
\t\t\tcolor: black;
\t\t}
\t</style>
{% endblock %}

{% block body %}

\t<main class=\"mainActu\">
\t\t<div class=\"news\">
\t\t\t<p class=\"titreActu\">
\t\t\t\tDernières actualités
\t\t\t</p>
\t\t</div>

\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t<a href=\"{{ path('app_actualite_new') }}\">
\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t</a>
\t\t{% endif %}

\t\t{% for actualite in actualites %}
\t\t\t<div class=\"containerNewArticle\">
\t\t\t\t<div class=\"border\"></div>
\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<div class=\"input-admin\">
                    <a title=\"Supprimer\" href=\"{{ path('app_actualite_delete', {'id': actualite.id}) }}\"><i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i></a>
\t\t\t\t\t<a href=\"{{ path('app_actualite_edit', {'id': actualite.id}) }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t\t<p class=\"titreActu2\">
\t\t\t\t\t{{ actualite.titreArt }}
\t\t\t\t</p>
\t\t\t\t<p class=\"dateActu\">
\t\t\t\t\t<small>Publié le :
\t\t\t\t\t\t{{ actualite.dateArt  ? actualite.dateArt |date('Y-m-d') : '' }}
\t\t\t\t\t</small>
\t\t\t\t</p>
\t\t\t\t<img class=\"imgActu\" src=\"/uploads/{{ actualite.imageActu }}\" alt=\"{{ actualite.titreArt }}\">
\t\t\t\t<p class=\"textActu\">
\t\t\t\t\t{{ actualite.descriptionArt }}
\t\t\t\t</p>
\t\t\t</div>
\t\t{% else %}
\t\t\t<p>
\t\t\t\tAucune actualité, veuillez en créer de nouvelles
\t\t\t</p>
\t\t{% endfor %}

\t\t<div class=\"navigation\">
\t\t\t{{ knp_pagination_render(actualites) }}
\t\t</div>

\t</main>

{% endblock %}
", "actualite/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/actualite/index.html.twig");
    }
}
