<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /header/header_admin.html.twig */
class __TwigTemplate_609ea325a24c4777bc8625712f505f3c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/header/header_admin.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/header/header_admin.html.twig"));

        // line 1
        echo " <header>
        <div class=\"header\">
            <div class=\"container_logo_header\">
                <div class=\"containerLeft\">
                    <div class=\"containerL_childeTop\">
                        <p>
                            <span class=\"borderYellow_header\">
                                BISON
                            </span>
                        </p>
                    </div>
                    <div class=\"containerL_childeLow\">
                        <p>
                            AFFÛTEUR
                        </p>
                    </div>
                </div>
                <div class=\"boxLogo\">
                    <img class=\"logo_header\" src=\"/images/logo.png\" alt=\"logo\"></div>
                    <div class=\"containerRight\">
                        <div class=\"containerR_childeTop\">
                            <p>
                                <span class=\"borderYellow_header\">
                                    AFFÛTÉ
                                </span>
                            </p>
                        </div>
                        <div class=\"containerR_childeLow\">
                            <p>
                                RÉMOULEUR
                            </p>
                        </div>
                    </div>
                </div>
                <div id=\"cont\">
                    <div class=\"l l1\"></div>
                    <div class=\"l l2\"></div>
                    <div class=\"l l3\"></div>
                </div>
                <nav id=\"nav\">
                    <ul id=\"ulHeader\" class='list_header ulHeader'>
                        <li>
                            <a href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_index");
        echo "\">
                                <p title=\"À Propos\">
                                    À Propos
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_index");
        echo "\">
                                <p title=\"Média\">
                                    Média
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_index");
        echo "\">
                                <p title=\"Modifier les coordonnées\">
                                    Modifier les coordonnées
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_index");
        echo "\">
                                <p title=\"Modifier les Prestations\">
                                    Modifier les Prestations
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_index");
        echo "\">
                                <p title=\"Modifier les Articles\">
                                    Modifier les Articles
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 78
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
        echo "\"><i title=\"Déconnexion\" class=\"fas fa-sign-out-alt fa-2x\"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
        </header>
    <script src=\"/js/header.js\"></script>";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "/header/header_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 78,  127 => 71,  117 => 64,  107 => 57,  97 => 50,  87 => 43,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" <header>
        <div class=\"header\">
            <div class=\"container_logo_header\">
                <div class=\"containerLeft\">
                    <div class=\"containerL_childeTop\">
                        <p>
                            <span class=\"borderYellow_header\">
                                BISON
                            </span>
                        </p>
                    </div>
                    <div class=\"containerL_childeLow\">
                        <p>
                            AFFÛTEUR
                        </p>
                    </div>
                </div>
                <div class=\"boxLogo\">
                    <img class=\"logo_header\" src=\"/images/logo.png\" alt=\"logo\"></div>
                    <div class=\"containerRight\">
                        <div class=\"containerR_childeTop\">
                            <p>
                                <span class=\"borderYellow_header\">
                                    AFFÛTÉ
                                </span>
                            </p>
                        </div>
                        <div class=\"containerR_childeLow\">
                            <p>
                                RÉMOULEUR
                            </p>
                        </div>
                    </div>
                </div>
                <div id=\"cont\">
                    <div class=\"l l1\"></div>
                    <div class=\"l l2\"></div>
                    <div class=\"l l3\"></div>
                </div>
                <nav id=\"nav\">
                    <ul id=\"ulHeader\" class='list_header ulHeader'>
                        <li>
                            <a href=\"{{ path(\"app_presentation_index\") }}\">
                                <p title=\"À Propos\">
                                    À Propos
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{ path(\"app_media_index\") }}\">
                                <p title=\"Média\">
                                    Média
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{ path(\"app_coordonnees_index\") }}\">
                                <p title=\"Modifier les coordonnées\">
                                    Modifier les coordonnées
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{ path(\"app_prestations_index\") }}\">
                                <p title=\"Modifier les Prestations\">
                                    Modifier les Prestations
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{ path(\"app_actualite_index\") }}\">
                                <p title=\"Modifier les Articles\">
                                    Modifier les Articles
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{ path(\"app_logout\") }}\"><i title=\"Déconnexion\" class=\"fas fa-sign-out-alt fa-2x\"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
        </header>
    <script src=\"/js/header.js\"></script>", "/header/header_admin.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/header/header_admin.html.twig");
    }
}
