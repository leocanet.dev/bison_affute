<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* prestations/index.html.twig */
class __TwigTemplate_45134ad9e89c5ede08e6d57a52ee5fd4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "prestations/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "prestations/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "prestations/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tPrestations


";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "\t<link rel=\"stylesheet\" href=\"/styles/services.css\">
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 13
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "
\t<main class=\"main_service\">
\t\t<div class=\"container_services\">

\t\t\t<div class=\"container_center\">
\t\t\t\t<img class=\"img_anim_service\" src=\"/images/lames.png\" alt=\"lames\">
\t\t\t</div>
\t\t\t<div class=\"container_right\">

\t\t\t\t<h2 class=\"gras\">
\t\t\t\t\tParticuliers & Professionnels
\t\t\t\t</h2>
\t\t\t\t<p>
\t\t\t\t\tJe vous accueille à mon atelier, pour effectuer des prestations d’affutâge.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de bouche:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tTraiteurs, bouchers, charcutiers, restaurateurs, poissonniers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tArtisans:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tCoiffeurs, couturiers, selliers, relieurs, cordonniers, tapissiers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de la nature:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tÉleveurs, agriculteurs, jardiniers,paysagistes.


\t\t\t\t</p>
\t\t\t</div>
\t\t</div>
\t\t<h2 class=\"h2Service\">
\t\t\tTarifs Affûtage 2022
\t\t</h2>

\t\t<div class=\"main_prix\">
\t\t\t";
        // line 72
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 73
            echo "\t\t\t<div class=\"textcenter\">
\t\t\t\t<a title=\"Nouveau\" href=\"";
            // line 74
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_new");
            echo "\">
\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black; margin-bottom: 1rem;\"></i>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t";
        }
        // line 79
        echo "\t\t\t<div class=\"container_Prix_Head2\">
\t\t\t\t<div class=\"container_prix\">
\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRESTATIONS


\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRIX


\t\t\t\t\t\t</p>
\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 94
        $context["brands"] = [];
        // line 95
        echo "\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prestations"]) || array_key_exists("prestations", $context) ? $context["prestations"] : (function () { throw new RuntimeError('Variable "prestations" does not exist.', 95, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["prestation"]) {
            // line 96
            echo "\t\t\t\t\t\t";
            if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 96), (isset($context["brands"]) || array_key_exists("brands", $context) ? $context["brands"] : (function () { throw new RuntimeError('Variable "brands" does not exist.', 96, $this->source); })()))) {
                // line 97
                echo "\t\t\t\t\t\t\t";
                $context["brands"] = twig_array_merge((isset($context["brands"]) || array_key_exists("brands", $context) ? $context["brands"] : (function () { throw new RuntimeError('Variable "brands" does not exist.', 97, $this->source); })()), [0 => twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 97)]);
                // line 98
                echo "\t\t\t\t\t\t\t<p class=\"titre_prestation\">
\t\t\t\t\t\t\t\t";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 99), "html", null, true);
                echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
            }
            // line 102
            echo "\t\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t\t<p style=\"word-break: break-all; width: 65%;\">
\t\t\t\t\t\t\t\t";
            // line 104
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "prestation", [], "any", false, false, false, 104), "html", null, true);
            echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t";
            // line 106
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 107
                echo "\t\t\t\t\t\t\t\t<a title=\"Modifier\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["prestation"], "id", [], "any", false, false, false, 107)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:yellow\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
            }
            // line 111
            echo "\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
            // line 112
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "prix", [], "any", false, false, false, 112), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t€


\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prestation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</main>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "prestations/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 119,  248 => 112,  245 => 111,  237 => 107,  235 => 106,  230 => 104,  226 => 102,  220 => 99,  217 => 98,  214 => 97,  211 => 96,  206 => 95,  204 => 94,  187 => 79,  179 => 74,  176 => 73,  174 => 72,  114 => 14,  104 => 13,  93 => 10,  83 => 9,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tPrestations


{% endblock %}

{% block stylesheets %}
\t<link rel=\"stylesheet\" href=\"/styles/services.css\">
{% endblock %}

{% block body %}

\t<main class=\"main_service\">
\t\t<div class=\"container_services\">

\t\t\t<div class=\"container_center\">
\t\t\t\t<img class=\"img_anim_service\" src=\"/images/lames.png\" alt=\"lames\">
\t\t\t</div>
\t\t\t<div class=\"container_right\">

\t\t\t\t<h2 class=\"gras\">
\t\t\t\t\tParticuliers & Professionnels
\t\t\t\t</h2>
\t\t\t\t<p>
\t\t\t\t\tJe vous accueille à mon atelier, pour effectuer des prestations d’affutâge.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de bouche:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tTraiteurs, bouchers, charcutiers, restaurateurs, poissonniers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tArtisans:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tCoiffeurs, couturiers, selliers, relieurs, cordonniers, tapissiers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de la nature:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tÉleveurs, agriculteurs, jardiniers,paysagistes.


\t\t\t\t</p>
\t\t\t</div>
\t\t</div>
\t\t<h2 class=\"h2Service\">
\t\t\tTarifs Affûtage 2022
\t\t</h2>

\t\t<div class=\"main_prix\">
\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t<div class=\"textcenter\">
\t\t\t\t<a title=\"Nouveau\" href=\"{{ path('app_prestations_new') }}\">
\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black; margin-bottom: 1rem;\"></i>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t{% endif %}
\t\t\t<div class=\"container_Prix_Head2\">
\t\t\t\t<div class=\"container_prix\">
\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRESTATIONS


\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRIX


\t\t\t\t\t\t</p>
\t\t\t\t\t</div>

\t\t\t\t\t{% set brands = [] %}
\t\t\t\t\t{% for prestation in prestations %}
\t\t\t\t\t\t{% if prestation.rubrique not in brands %}
\t\t\t\t\t\t\t{% set brands = brands|merge([prestation.rubrique]) %}
\t\t\t\t\t\t\t<p class=\"titre_prestation\">
\t\t\t\t\t\t\t\t{{ prestation.rubrique }}
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t\t<p style=\"word-break: break-all; width: 65%;\">
\t\t\t\t\t\t\t\t{{ prestation.prestation }}
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t\t\t\t\t<a title=\"Modifier\" href=\"{{ path('app_prestations_edit', {'id': prestation.id}) }}\">
\t\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:yellow\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t{{ prestation.prix }}
\t\t\t\t\t\t\t\t€


\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</main>

{% endblock %}
", "prestations/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/prestations/index.html.twig");
    }
}
