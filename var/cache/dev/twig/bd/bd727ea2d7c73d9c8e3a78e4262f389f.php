<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /header/header.html.twig */
class __TwigTemplate_9736f05cc64ece442e656646957d1c74 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/header/header.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/header/header.html.twig"));

        // line 1
        echo "<header>
\t<div class=\"header\">
\t\t<div class=\"container_logo_header\">
\t\t\t<div class=\"containerLeft\">
\t\t\t\t<div class=\"containerL_childeTop\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\t\tBISON
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"containerL_childeLow\">
\t\t\t\t\t<p>
\t\t\t\t\t\tAFFÛTEUR
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"boxLogo\">
\t\t\t\t<img class=\"logo_header\" src=\"/images/logo.png\" alt=\"logo\"></div>
\t\t\t<div class=\"containerRight\">
\t\t\t\t<div class=\"containerR_childeTop\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\t\tAFFÛTÉ
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"containerR_childeLow\">
\t\t\t\t\t<p>
\t\t\t\t\t\tRÉMOULEUR
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div id=\"cont\">
\t\t\t<div class=\"l l1\"></div>
\t\t\t<div class=\"l l2\"></div>
\t\t\t<div class=\"l l3\"></div>
\t\t</div>
\t\t<nav id=\"nav\">
\t\t\t<ul id=\"ulHeader\" class='list_header ulHeader'>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Accueil\" href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_index");
        echo "\">
\t\t\t\t\t\tAccueil
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Mes services\" href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_index");
        echo "\">
\t\t\t\t\t\tMes services
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"À propos\" href=\"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_index");
        echo "\">
\t\t\t\t\t\tÀ propos
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Galerie\" href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_index");
        echo "\">
\t\t\t\t\t\tGalerie
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Contact\" href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_index");
        echo "\">
\t\t\t\t\t\tContact
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Actualités\" href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_index");
        echo "\">
\t\t\t\t\t\tActualités
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</nav>
\t</div>
\t<div id=\"CHH\" class=\"containerHeaderHidden\">
\t\t<ul id=\"ulHiddenHeader\" class='listHideen_header ulHeader'>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 78
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_index");
        echo "\">
\t\t\t\t\tAccueil
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_index");
        echo "\">
\t\t\t\t\tMes services
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_index");
        echo "\">
\t\t\t\t\tA propos
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_index");
        echo "\">
\t\t\t\t\tGalerie
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_index");
        echo "\">
\t\t\t\t\tContact
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"";
        // line 103
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_index");
        echo "\">
\t\t\t\t\tActualites
\t\t\t\t</a>
\t\t\t</li>
\t\t</ul>
\t</div>
\t<script src=\"/js/header.js\"></script>
</header>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "/header/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 103,  172 => 98,  164 => 93,  156 => 88,  148 => 83,  140 => 78,  127 => 68,  119 => 63,  111 => 58,  103 => 53,  95 => 48,  87 => 43,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header>
\t<div class=\"header\">
\t\t<div class=\"container_logo_header\">
\t\t\t<div class=\"containerLeft\">
\t\t\t\t<div class=\"containerL_childeTop\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\t\tBISON
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"containerL_childeLow\">
\t\t\t\t\t<p>
\t\t\t\t\t\tAFFÛTEUR
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"boxLogo\">
\t\t\t\t<img class=\"logo_header\" src=\"/images/logo.png\" alt=\"logo\"></div>
\t\t\t<div class=\"containerRight\">
\t\t\t\t<div class=\"containerR_childeTop\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\t\tAFFÛTÉ
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"containerR_childeLow\">
\t\t\t\t\t<p>
\t\t\t\t\t\tRÉMOULEUR
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div id=\"cont\">
\t\t\t<div class=\"l l1\"></div>
\t\t\t<div class=\"l l2\"></div>
\t\t\t<div class=\"l l3\"></div>
\t\t</div>
\t\t<nav id=\"nav\">
\t\t\t<ul id=\"ulHeader\" class='list_header ulHeader'>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Accueil\" href=\"{{ path('app_index') }}\">
\t\t\t\t\t\tAccueil
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Mes services\" href=\"{{ path('app_prestations_index') }}\">
\t\t\t\t\t\tMes services
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"À propos\" href=\"{{ path('app_presentation_index') }}\">
\t\t\t\t\t\tÀ propos
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Galerie\" href=\"{{ path('app_media_index') }}\">
\t\t\t\t\t\tGalerie
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Contact\" href=\"{{ path('app_coordonnees_index') }}\">
\t\t\t\t\t\tContact
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a title=\"Actualités\" href=\"{{ path('app_actualite_index') }}\">
\t\t\t\t\t\tActualités
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</nav>
\t</div>
\t<div id=\"CHH\" class=\"containerHeaderHidden\">
\t\t<ul id=\"ulHiddenHeader\" class='listHideen_header ulHeader'>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_index') }}\">
\t\t\t\t\tAccueil
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_prestations_index') }}\">
\t\t\t\t\tMes services
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_presentation_index') }}\">
\t\t\t\t\tA propos
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_media_index') }}\">
\t\t\t\t\tGalerie
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_coordonnees_index') }}\">
\t\t\t\t\tContact
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"li_header_hideen\">
\t\t\t\t<a href=\"{{ path('app_actualite_index') }}\">
\t\t\t\t\tActualites
\t\t\t\t</a>
\t\t\t</li>
\t\t</ul>
\t</div>
\t<script src=\"/js/header.js\"></script>
</header>
", "/header/header.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/header/header.html.twig");
    }
}
