<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index/index.html.twig */
class __TwigTemplate_afdf0ead8c28fde9f64b9341e9590b06 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tAccueil
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "\t<main class=\"index\">
\t\t<div class=\"head\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"intro-text\">
\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t<p class=\"telTop\">
\t\t\t\t\t\t\t<a title=\"Téléphone\" href=\"tel:06.09.57.58.48\">
\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-phone yellow\"></i>
\t\t\t\t\t\t\t\t06.09.57.58.48
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"intro-heading\"></div>
\t\t\t\t\t<h1 class=\"word1\">

\t\t\t\t\t\tAffutage toutes lames

\t\t\t\t\t</h1>
\t\t\t\t\t<h2 class=\"word2\">
\t\t\t\t\t\t<span class=\"blur1\">
\t\t\t\t\t\t\tet
\t\t\t\t\t\t</span>
\t\t\t\t\t</h2>
\t\t\t\t\t<h3 class=\"word3\">
\t\t\t\t\t\t<span class=\"blur1\">
\t\t\t\t\t\t\ttout objet tranchant
\t\t\t\t\t\t</span>
\t\t\t\t\t</h3>
\t\t\t\t\t<i class='fas fa-angle-down arrow' style='font-size:36px; color:yellow'></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"section1\" class=\"container-fluid\">
\t\t\t<div id=\"section3\" class=\"container-fluid\">
\t\t\t\t<h2 class=\"metier\">
\t\t\t\t\t<u>
\t\t\t\t\t\tDécouvrez mon métier
\t\t\t\t\t</u>
\t\t\t\t</h2>
\t\t\t</div>
\t\t\t<div class=\"mainMidle\">
\t\t\t\t<h3 class=\"text1Index\">
\t\t\t\t\tPrenez rendez-vous et venez faire affûter vos outils dans la commune de SAINT-NAZAIRE, par un
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                professionnel !
\t\t\t\t</h3>
\t\t\t\t<hr>
\t\t\t\t<h3 class=\"text2Index\">
\t\t\t\t\tJ’affute vos couteaux, ciseaux, ciseaux à bois, sécateurs, cisailles haies, taille-haies thermiques ou
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                    électriques,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                    les lames de coupe jambon, les chaînes de tronçonneuse.
\t\t\t\t</h3>
\t\t\t\t<hr>

\t\t\t\t<div class=\"container2\">
\t\t\t\t\t<div class=\"contact\">
\t\t\t\t\t\t<a class=\"button\" href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_index");
        echo "\" title=\"Contactez-nous\">
\t\t\t\t\t\t\tContactez nous
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<img src=\"https://www.bisonaffute.fr/img/lames.png\" id=\"lames\" alt=\"lame\"></div>

\t\t</div>

\t\t<div id=\"section2\" class=\"container-fluid\">
\t\t\t<p class=\"metier\">
\t\t\t\t<u>
\t\t\t\t\tOù me trouver...
\t\t\t\t</u>
\t\t\t</p>
\t\t\t<div class=\"flex\">
\t\t\t\t<div class=\"column\">
\t\t\t\t\t<iframe title=\"Adresse de mon atelier\" class=\"iframeIndex\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1221.9867737380519!2d2.897210462092828!3d43.24323220259002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1b137e8485891%3A0x2d1d2d79ce3e5709!2zQmlzb24gQWZmw7t0w6ksIGFmZnV0ZXVyIHLDqW1vdWxldXI!5e0!3m2!1sfr!2sfr!4v1643364470812!5m2!1sfr!2sfr\" width=\"600\" height=\"450\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
\t\t\t\t</div>

\t\t\t\t<div class=\"top_index\">
\t\t\t\t\t<div id=\"open\"></div>
\t\t\t\t\t<div id=\"currently\">
\t\t\t\t\t\t<p id=\"currently_open\">
\t\t\t\t\t\t\tBison Affûté
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"currently2\">
\t\t\t\t\t\t<p class=\"Semain\">
\t\t\t\t\t\t\tLundi-Vendredi:
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"time1\">
\t\t\t\t\t\t\t09:00 - 12:00
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"time2\">
\t\t\t\t\t\t\t14:00 - 18:00
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"red\">
\t\t\t\t\t\t\tSamedi: FERMÉ
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"red\">
\t\t\t\t\t\t\tDimanche:FERMÉ
\t\t\t\t\t\t</p>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t</div>
\t</main>
\t<script src=\"/js/index.js\"></script>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 64,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tAccueil
{% endblock %}

{% block body %}
\t<main class=\"index\">
\t\t<div class=\"head\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"intro-text\">
\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t<p class=\"telTop\">
\t\t\t\t\t\t\t<a title=\"Téléphone\" href=\"tel:06.09.57.58.48\">
\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-phone yellow\"></i>
\t\t\t\t\t\t\t\t06.09.57.58.48
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"intro-heading\"></div>
\t\t\t\t\t<h1 class=\"word1\">

\t\t\t\t\t\tAffutage toutes lames

\t\t\t\t\t</h1>
\t\t\t\t\t<h2 class=\"word2\">
\t\t\t\t\t\t<span class=\"blur1\">
\t\t\t\t\t\t\tet
\t\t\t\t\t\t</span>
\t\t\t\t\t</h2>
\t\t\t\t\t<h3 class=\"word3\">
\t\t\t\t\t\t<span class=\"blur1\">
\t\t\t\t\t\t\ttout objet tranchant
\t\t\t\t\t\t</span>
\t\t\t\t\t</h3>
\t\t\t\t\t<i class='fas fa-angle-down arrow' style='font-size:36px; color:yellow'></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"section1\" class=\"container-fluid\">
\t\t\t<div id=\"section3\" class=\"container-fluid\">
\t\t\t\t<h2 class=\"metier\">
\t\t\t\t\t<u>
\t\t\t\t\t\tDécouvrez mon métier
\t\t\t\t\t</u>
\t\t\t\t</h2>
\t\t\t</div>
\t\t\t<div class=\"mainMidle\">
\t\t\t\t<h3 class=\"text1Index\">
\t\t\t\t\tPrenez rendez-vous et venez faire affûter vos outils dans la commune de SAINT-NAZAIRE, par un
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                professionnel !
\t\t\t\t</h3>
\t\t\t\t<hr>
\t\t\t\t<h3 class=\"text2Index\">
\t\t\t\t\tJ’affute vos couteaux, ciseaux, ciseaux à bois, sécateurs, cisailles haies, taille-haies thermiques ou
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                    électriques,
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t                                    les lames de coupe jambon, les chaînes de tronçonneuse.
\t\t\t\t</h3>
\t\t\t\t<hr>

\t\t\t\t<div class=\"container2\">
\t\t\t\t\t<div class=\"contact\">
\t\t\t\t\t\t<a class=\"button\" href=\"{{ path(\"app_coordonnees_index\")}}\" title=\"Contactez-nous\">
\t\t\t\t\t\t\tContactez nous
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<img src=\"https://www.bisonaffute.fr/img/lames.png\" id=\"lames\" alt=\"lame\"></div>

\t\t</div>

\t\t<div id=\"section2\" class=\"container-fluid\">
\t\t\t<p class=\"metier\">
\t\t\t\t<u>
\t\t\t\t\tOù me trouver...
\t\t\t\t</u>
\t\t\t</p>
\t\t\t<div class=\"flex\">
\t\t\t\t<div class=\"column\">
\t\t\t\t\t<iframe title=\"Adresse de mon atelier\" class=\"iframeIndex\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1221.9867737380519!2d2.897210462092828!3d43.24323220259002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1b137e8485891%3A0x2d1d2d79ce3e5709!2zQmlzb24gQWZmw7t0w6ksIGFmZnV0ZXVyIHLDqW1vdWxldXI!5e0!3m2!1sfr!2sfr!4v1643364470812!5m2!1sfr!2sfr\" width=\"600\" height=\"450\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
\t\t\t\t</div>

\t\t\t\t<div class=\"top_index\">
\t\t\t\t\t<div id=\"open\"></div>
\t\t\t\t\t<div id=\"currently\">
\t\t\t\t\t\t<p id=\"currently_open\">
\t\t\t\t\t\t\tBison Affûté
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"currently2\">
\t\t\t\t\t\t<p class=\"Semain\">
\t\t\t\t\t\t\tLundi-Vendredi:
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"time1\">
\t\t\t\t\t\t\t09:00 - 12:00
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"time2\">
\t\t\t\t\t\t\t14:00 - 18:00
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"red\">
\t\t\t\t\t\t\tSamedi: FERMÉ
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"red\">
\t\t\t\t\t\t\tDimanche:FERMÉ
\t\t\t\t\t\t</p>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t</div>
\t</main>
\t<script src=\"/js/index.js\"></script>

{% endblock %}
", "index/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/index/index.html.twig");
    }
}
