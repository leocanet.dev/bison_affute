<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /footer/footer.html.twig */
class __TwigTemplate_05e6940b70f84f7533947fbd2441228d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/footer/footer.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/footer/footer.html.twig"));

        // line 1
        echo "
    <div id=\"divTopFooterKK\"></div>
    <footer>
        <div class=\"bigDivFooter\">
            <div class=\"listFooter\" id=\"containerLeftFooter\">
                <ul class=\"classUlFooter\">
                    <li class=\"liFooter\">
                        <a title=\"Accueil\" href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_index");
        echo "\">
                            Accueil
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Mes services\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_index");
        echo "\">
                            Mes services
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"À propos\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_index");
        echo "\">
                            À propos
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Galerie\" href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_index");
        echo "\">
                            Galerie
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Contact\" href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_coordonnees_index");
        echo "\">
                            Contact
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Actualités\" href=\"";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_index");
        echo "\">
                            Actualités
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class=\"bigDivFooter\">
            <div id=\"containerMiddleFooter1\">
                <div class=\"divMiddleLink\">
                    <a title=\"Facebook\" href=\"https://www.facebook.com/Bison.affute11\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_facebook.png\" alt=\"logo_facebook
                                                    \" />
                    </a>
                </div>
                <div class=\"divMiddleLink\">
                    <a title=\"Youtube\" href=\"https://www.youtube.com/channel/UCCskTYJncIHcUikjWXHhaGw\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_youtube.png\" alt=\"logo_youtube
                                                    \" />
                    </a>
                </div>
                <div class=\"divMiddleLink\">
                    <a title=\"Instagram\" href=\"https://www.instagram.com/?hl=en\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_instagram.png\" alt=\"logo_instagram
                                                    \" />
                    </a>
                </div>
            </div>
            <div id=\"containerMiddleFooter2\">
                <div class=\"divMiddleCopyRight\">
                    <p>
                        Copyright &copy; 2022 Simplon.co, All rights reserved.
                    </p>
                </div>
            </div>
        </div>
        <div class=\"bigDivFooter\" id=\"divFooterRightContact\">
            <div class=\"containerRightFooter\">
                <p id=\"numFooter\">
                    <a href=\"tel:06.09.57.58.48\">
                        <i class=\"fa-solid fa-phone yellow\"></i> 06.09.57.58.48
                    </a>
                </p>
            </div>
            <div class=\"containerRightFooter\">
                <p id=\"adresseFooter\">
                    <a href=\"https://goo.gl/maps/ACmyF2HPXL3sBj7B9\">
                        <i class=\"fa-solid fa-location-dot yellow\"></i> SAINT-NAZAIRE
                                            D’AUDE
                    </a>
                </p>

            </div>
        </div>
    </footer>
   ";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "/footer/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 33,  84 => 28,  76 => 23,  68 => 18,  60 => 13,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
    <div id=\"divTopFooterKK\"></div>
    <footer>
        <div class=\"bigDivFooter\">
            <div class=\"listFooter\" id=\"containerLeftFooter\">
                <ul class=\"classUlFooter\">
                    <li class=\"liFooter\">
                        <a title=\"Accueil\" href=\"{{ path('app_index') }}\">
                            Accueil
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Mes services\" href=\"{{ path('app_prestations_index') }}\">
                            Mes services
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"À propos\" href=\"{{ path('app_presentation_index') }}\">
                            À propos
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Galerie\" href=\"{{ path('app_media_index') }}\">
                            Galerie
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Contact\" href=\"{{ path('app_coordonnees_index') }}\">
                            Contact
                        </a>
                    </li>
                    <li class=\"liFooter\">
                        <a title=\"Actualités\" href=\"{{ path('app_actualite_index') }}\">
                            Actualités
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class=\"bigDivFooter\">
            <div id=\"containerMiddleFooter1\">
                <div class=\"divMiddleLink\">
                    <a title=\"Facebook\" href=\"https://www.facebook.com/Bison.affute11\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_facebook.png\" alt=\"logo_facebook
                                                    \" />
                    </a>
                </div>
                <div class=\"divMiddleLink\">
                    <a title=\"Youtube\" href=\"https://www.youtube.com/channel/UCCskTYJncIHcUikjWXHhaGw\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_youtube.png\" alt=\"logo_youtube
                                                    \" />
                    </a>
                </div>
                <div class=\"divMiddleLink\">
                    <a title=\"Instagram\" href=\"https://www.instagram.com/?hl=en\">
                        <img class=\"logoFooterLink\" width=\"50\" src=\"/images/logo_instagram.png\" alt=\"logo_instagram
                                                    \" />
                    </a>
                </div>
            </div>
            <div id=\"containerMiddleFooter2\">
                <div class=\"divMiddleCopyRight\">
                    <p>
                        Copyright &copy; 2022 Simplon.co, All rights reserved.
                    </p>
                </div>
            </div>
        </div>
        <div class=\"bigDivFooter\" id=\"divFooterRightContact\">
            <div class=\"containerRightFooter\">
                <p id=\"numFooter\">
                    <a href=\"tel:06.09.57.58.48\">
                        <i class=\"fa-solid fa-phone yellow\"></i> 06.09.57.58.48
                    </a>
                </p>
            </div>
            <div class=\"containerRightFooter\">
                <p id=\"adresseFooter\">
                    <a href=\"https://goo.gl/maps/ACmyF2HPXL3sBj7B9\">
                        <i class=\"fa-solid fa-location-dot yellow\"></i> SAINT-NAZAIRE
                                            D’AUDE
                    </a>
                </p>

            </div>
        </div>
    </footer>
   ", "/footer/footer.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/footer/footer.html.twig");
    }
}
