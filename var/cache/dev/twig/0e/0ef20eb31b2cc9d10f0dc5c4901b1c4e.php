<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* media/index.html.twig */
class __TwigTemplate_88e81a085ed9d22e02a6b3c11e9a84bb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "media/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "media/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "media/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tMedia
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "\t<main id=\"bodygalerie\">
\t\t<h2 class=\"h1\">
\t\t\tGalerie Photos et Vidéos
\t\t</h2>

\t\t";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 14
            echo "\t\t\t<div class=\"textcenter\">
\t\t\t\t<a title=\"Nouveau\" href=\"";
            // line 15
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_new");
            echo "\">
\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x fa-icon\" style=\"color:black\"></i>
\t\t\t\t</a>
\t\t\t</div>
\t\t";
        }
        // line 20
        echo "
\t\t<div class=\"mesphotos\">

\t\t\t";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["media"]) || array_key_exists("media", $context) ? $context["media"] : (function () { throw new RuntimeError('Variable "media" does not exist.', 23, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["medium"]) {
            // line 24
            echo "
\t\t\t\t";
            // line 25
            if (twig_get_attribute($this->env, $this->source, $context["medium"], "image", [], "any", false, false, false, 25)) {
                // line 26
                echo "\t\t\t\t\t<img class=\"imagesbdd\" src=\"/uploads/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "image", [], "any", false, false, false, 26), "html", null, true);
                echo "\" alt=\"photofilm\">
\t\t\t\t";
            }
            // line 28
            echo "
\t\t\t\t";
            // line 29
            if (twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 29)) {
                // line 30
                echo "\t\t\t\t\t<video controls style=\"width: 25vw; height: 30vh\">
\t\t\t\t\t\t<source src=\"/uploads/";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 31), "html", null, true);
                echo "\" type=\"video/webm\">
\t\t\t\t\t\t<source src=\"/uploads/";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 32), "html", null, true);
                echo "\" type=\"video/mp4\">
\t\t\t\t\t\tSorry, your browser doesn't support embedded videos.
\t\t\t\t\t</video>
\t\t\t\t\t<a title=\"Supprimer\" href=\"";
                // line 35
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 35)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t";
            }
            // line 39
            echo "\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 40
                echo "\t\t\t\t<div class=\"input-admin\">
\t\t\t\t\t<a title=\"Supprimer\" href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 44)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t";
            }
            // line 49
            echo "\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 50
            echo "\t\t\t\t<p>
\t\t\t\t\tAucune photo(s) ou video(s) n'est disponible
\t\t\t\t</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['medium'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "\t\t</div>

\t\t<div class=\"separation\">
\t\t\t<img class=\"separe\" src=\"/images/lames.png\" alt=\"lames\">
\t\t</div>
\t\t<div id=\"mesvideos\">
\t\t\t";
        // line 60
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 61
            echo "\t\t\t\t<form action=\"/URL\" method=\"post\">
\t\t\t\t\t<label for=\"url\">URL</label>
\t\t\t\t\t<input name=\"url\" type=\"text\">
\t\t\t\t\t<button type=\"submit\">OK</button>
\t\t\t\t</form>
\t\t\t";
        }
        // line 67
        echo "        <iframe width=\"800\"height=\"600\" src=\"https://www.youtube.com/embed/";
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 67, $this->source); })()), "html", null, true);
        echo "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t</div>
\t</main>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "media/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 67,  200 => 61,  198 => 60,  190 => 54,  181 => 50,  176 => 49,  168 => 44,  162 => 41,  159 => 40,  156 => 39,  149 => 35,  143 => 32,  139 => 31,  136 => 30,  134 => 29,  131 => 28,  125 => 26,  123 => 25,  120 => 24,  115 => 23,  110 => 20,  102 => 15,  99 => 14,  97 => 13,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tMedia
{% endblock %}

{% block body %}
\t<main id=\"bodygalerie\">
\t\t<h2 class=\"h1\">
\t\t\tGalerie Photos et Vidéos
\t\t</h2>

\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t<div class=\"textcenter\">
\t\t\t\t<a title=\"Nouveau\" href=\"{{ path('app_media_new') }}\">
\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x fa-icon\" style=\"color:black\"></i>
\t\t\t\t</a>
\t\t\t</div>
\t\t{% endif %}

\t\t<div class=\"mesphotos\">

\t\t\t{% for medium in media %}

\t\t\t\t{% if medium.image %}
\t\t\t\t\t<img class=\"imagesbdd\" src=\"/uploads/{{ medium.image }}\" alt=\"photofilm\">
\t\t\t\t{% endif %}

\t\t\t\t{% if medium.video %}
\t\t\t\t\t<video controls style=\"width: 25vw; height: 30vh\">
\t\t\t\t\t\t<source src=\"/uploads/{{ medium.video }}\" type=\"video/webm\">
\t\t\t\t\t\t<source src=\"/uploads/{{ medium.video }}\" type=\"video/mp4\">
\t\t\t\t\t\tSorry, your browser doesn't support embedded videos.
\t\t\t\t\t</video>
\t\t\t\t\t<a title=\"Supprimer\" href=\"{{ path('app_media_delete', {'id': medium.id}) }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t{% endif %}
\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<div class=\"input-admin\">
\t\t\t\t\t<a title=\"Supprimer\" href=\"{{ path('app_media_delete', {'id': medium.id}) }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"{{ path('app_media_edit', {'id': medium.id}) }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t{% else %}
\t\t\t\t<p>
\t\t\t\t\tAucune photo(s) ou video(s) n'est disponible
\t\t\t\t</p>
\t\t\t{% endfor %}
\t\t</div>

\t\t<div class=\"separation\">
\t\t\t<img class=\"separe\" src=\"/images/lames.png\" alt=\"lames\">
\t\t</div>
\t\t<div id=\"mesvideos\">
\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<form action=\"/URL\" method=\"post\">
\t\t\t\t\t<label for=\"url\">URL</label>
\t\t\t\t\t<input name=\"url\" type=\"text\">
\t\t\t\t\t<button type=\"submit\">OK</button>
\t\t\t\t</form>
\t\t\t{% endif %}
        <iframe width=\"800\"height=\"600\" src=\"https://www.youtube.com/embed/{{ url }}\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t</div>
\t</main>

{% endblock %}
", "media/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/media/index.html.twig");
    }
}
