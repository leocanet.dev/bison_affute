<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* presentation/index.html.twig */
class __TwigTemplate_b3dab0e25ac243b2738757ae7e67abdd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "presentation/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "presentation/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "presentation/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Présentation
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t<div id=\"bodyProposDiv\">
\t\t<main class=\"mainPropos\">

\t\t\t";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["presentations"]) || array_key_exists("presentations", $context) ? $context["presentations"] : (function () { throw new RuntimeError('Variable "presentations" does not exist.', 12, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["presentation"]) {
            // line 13
            echo "\t\t\t\t<h2 class=\"h1Apropos\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "titrePres", [], "any", false, false, false, 13), "html", null, true);
            echo "</h2>
\t\t\t\t";
            // line 14
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 15
                echo "\t\t\t\t<div class=\"input-admin\">
\t\t\t\t\t<a title=\"Nouveau\" href=\"";
                // line 16
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_new");
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["presentation"], "id", [], "any", false, false, false, 19)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t";
            }
            // line 24
            echo "
\t\t\t\t<p class=\"textPropos\">";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe1Pres", [], "any", false, false, false, 25), "html", null, true);
            echo "</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "ImagePres1", [], "any", false, false, false, 26), "html", null, true);
            echo "\" alt=\"Premiere image\"/>
\t\t\t\t<p class=\"textPropos\">";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe2Pres", [], "any", false, false, false, 27), "html", null, true);
            echo "</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "ImagePres2", [], "any", false, false, false, 28), "html", null, true);
            echo "\" alt=\"Deuxieme image\"/>
\t\t\t\t<p class=\"textPropos\">";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe3Pres", [], "any", false, false, false, 29), "html", null, true);
            echo "</p>

\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 32
            echo "            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 33
                echo "                <a title=\"Nouveau\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_new");
                echo "\">
                    <i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
                </a>
            ";
            }
            // line 37
            echo "\t\t\t\t<p>Aucune description, veuillez créer une description</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['presentation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "\t\t</main>
\t</div>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "presentation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 39,  162 => 37,  154 => 33,  151 => 32,  143 => 29,  139 => 28,  135 => 27,  131 => 26,  127 => 25,  124 => 24,  116 => 19,  110 => 16,  107 => 15,  105 => 14,  100 => 13,  95 => 12,  89 => 8,  79 => 7,  59 => 4,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}


{% block title %}Présentation
{% endblock %}

{% block body %}

\t<div id=\"bodyProposDiv\">
\t\t<main class=\"mainPropos\">

\t\t\t{% for presentation in presentations %}
\t\t\t\t<h2 class=\"h1Apropos\">{{ presentation.titrePres }}</h2>
\t\t\t\t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<div class=\"input-admin\">
\t\t\t\t\t<a title=\"Nouveau\" href=\"{{ path('app_presentation_new') }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"{{ path('app_presentation_edit', {'id': presentation.id}) }}\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t{% endif %}

\t\t\t\t<p class=\"textPropos\">{{ presentation.paragraphe1Pres }}</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/{{ presentation.ImagePres1 }}\" alt=\"Premiere image\"/>
\t\t\t\t<p class=\"textPropos\">{{ presentation.paragraphe2Pres }}</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/{{ presentation.ImagePres2 }}\" alt=\"Deuxieme image\"/>
\t\t\t\t<p class=\"textPropos\">{{ presentation.paragraphe3Pres }}</p>

\t\t\t{% else %}
            {% if is_granted('ROLE_ADMIN') %}
                <a title=\"Nouveau\" href=\"{{ path('app_presentation_new') }}\">
                    <i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
                </a>
            {% endif %}
\t\t\t\t<p>Aucune description, veuillez créer une description</p>
\t\t\t{% endfor %}
\t\t</main>
\t</div>

{% endblock %}
", "presentation/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/presentation/index.html.twig");
    }
}
