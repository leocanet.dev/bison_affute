<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/actualite' => [[['_route' => 'app_actualite_index', '_controller' => 'App\\Controller\\ActualiteController::index'], null, ['GET' => 0], null, true, false, null]],
        '/actualite/new' => [[['_route' => 'app_actualite_new', '_controller' => 'App\\Controller\\ActualiteController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/coordonnees' => [[['_route' => 'app_coordonnees_index', '_controller' => 'App\\Controller\\CoordonneesController::index'], null, ['GET' => 0], null, true, false, null]],
        '/coordonnees/new' => [[['_route' => 'app_coordonnees_new', '_controller' => 'App\\Controller\\CoordonneesController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/footer' => [[['_route' => 'app_footer', '_controller' => 'App\\Controller\\FooterController::index'], null, null, null, false, false, null]],
        '/header' => [[['_route' => 'app_header', '_controller' => 'App\\Controller\\HeaderController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/media' => [[['_route' => 'app_media_index', '_controller' => 'App\\Controller\\MediaController::index'], null, ['GET' => 0], null, true, false, null]],
        '/media/new' => [[['_route' => 'app_media_new', '_controller' => 'App\\Controller\\MediaController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/presentation' => [[['_route' => 'app_presentation_index', '_controller' => 'App\\Controller\\PresentationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/presentation/new' => [[['_route' => 'app_presentation_new', '_controller' => 'App\\Controller\\PresentationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/prestations' => [[['_route' => 'app_prestations_index', '_controller' => 'App\\Controller\\PrestationsController::index'], null, ['GET' => 0], null, true, false, null]],
        '/prestations/new' => [[['_route' => 'app_prestations_new', '_controller' => 'App\\Controller\\PrestationsController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/deconnexion' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/URL' => [[['_route' => 'app_VideoYouTube', '_controller' => 'App\\Controller\\VideoYouTubeController::index'], null, ['POST' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/actualite/(?'
                    .'|([^/]++)(?'
                        .'|(*:194)'
                        .'|/edit(*:207)'
                    .')'
                    .'|delete/([^/]++)(*:231)'
                .')'
                .'|/coordonnees/([^/]++)(?'
                    .'|(*:264)'
                    .'|/edit(*:277)'
                    .'|(*:285)'
                .')'
                .'|/media/(?'
                    .'|([^/]++)(?'
                        .'|(*:315)'
                        .'|/edit(*:328)'
                    .')'
                    .'|delete/([^/]++)(*:352)'
                .')'
                .'|/pres(?'
                    .'|entation/([^/]++)(?'
                        .'|(*:389)'
                        .'|/edit(*:402)'
                        .'|(*:410)'
                    .')'
                    .'|tations/([^/]++)(?'
                        .'|(*:438)'
                        .'|/edit(*:451)'
                        .'|(*:459)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        194 => [[['_route' => 'app_actualite_show', '_controller' => 'App\\Controller\\ActualiteController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        207 => [[['_route' => 'app_actualite_edit', '_controller' => 'App\\Controller\\ActualiteController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        231 => [[['_route' => 'app_actualite_delete', '_controller' => 'App\\Controller\\ActualiteController::delete'], ['id'], ['GET' => 0], null, false, true, null]],
        264 => [[['_route' => 'app_coordonnees_show', '_controller' => 'App\\Controller\\CoordonneesController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        277 => [[['_route' => 'app_coordonnees_edit', '_controller' => 'App\\Controller\\CoordonneesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        285 => [[['_route' => 'app_coordonnees_delete', '_controller' => 'App\\Controller\\CoordonneesController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        315 => [[['_route' => 'app_media_show', '_controller' => 'App\\Controller\\MediaController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        328 => [[['_route' => 'app_media_edit', '_controller' => 'App\\Controller\\MediaController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        352 => [[['_route' => 'app_media_delete', '_controller' => 'App\\Controller\\MediaController::delete'], ['id'], ['GET' => 0], null, false, true, null]],
        389 => [[['_route' => 'app_presentation_show', '_controller' => 'App\\Controller\\PresentationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        402 => [[['_route' => 'app_presentation_edit', '_controller' => 'App\\Controller\\PresentationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        410 => [[['_route' => 'app_presentation_delete', '_controller' => 'App\\Controller\\PresentationController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        438 => [[['_route' => 'app_prestations_show', '_controller' => 'App\\Controller\\PrestationsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        451 => [[['_route' => 'app_prestations_edit', '_controller' => 'App\\Controller\\PrestationsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        459 => [
            [['_route' => 'app_prestations_delete', '_controller' => 'App\\Controller\\PrestationsController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
