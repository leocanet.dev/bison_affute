<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/actualite' => [[['_route' => 'app_actualite_index', '_controller' => 'App\\Controller\\ActualiteController::index'], null, ['GET' => 0], null, true, false, null]],
        '/actualite/new' => [[['_route' => 'app_actualite_new', '_controller' => 'App\\Controller\\ActualiteController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/coordonnees' => [[['_route' => 'app_coordonnees_index', '_controller' => 'App\\Controller\\CoordonneesController::index'], null, ['GET' => 0], null, true, false, null]],
        '/coordonnees/new' => [[['_route' => 'app_coordonnees_new', '_controller' => 'App\\Controller\\CoordonneesController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/footer' => [[['_route' => 'app_footer', '_controller' => 'App\\Controller\\FooterController::index'], null, null, null, false, false, null]],
        '/header' => [[['_route' => 'app_header', '_controller' => 'App\\Controller\\HeaderController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/media' => [[['_route' => 'app_media_index', '_controller' => 'App\\Controller\\MediaController::index'], null, ['GET' => 0], null, true, false, null]],
        '/media/new' => [[['_route' => 'app_media_new', '_controller' => 'App\\Controller\\MediaController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/presentation' => [[['_route' => 'app_presentation_index', '_controller' => 'App\\Controller\\PresentationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/presentation/new' => [[['_route' => 'app_presentation_new', '_controller' => 'App\\Controller\\PresentationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/prestations' => [[['_route' => 'app_prestations_index', '_controller' => 'App\\Controller\\PrestationsController::index'], null, ['GET' => 0], null, true, false, null]],
        '/prestations/new' => [[['_route' => 'app_prestations_new', '_controller' => 'App\\Controller\\PrestationsController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/deconnexion' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/URL' => [[['_route' => 'app_VideoYouTube', '_controller' => 'App\\Controller\\VideoYouTubeController::index'], null, ['POST' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/actualite/(?'
                    .'|([^/]++)(?'
                        .'|(*:32)'
                        .'|/edit(*:44)'
                    .')'
                    .'|delete/([^/]++)(*:67)'
                .')'
                .'|/coordonnees/([^/]++)(?'
                    .'|(*:99)'
                    .'|/edit(*:111)'
                    .'|(*:119)'
                .')'
                .'|/media/(?'
                    .'|([^/]++)(?'
                        .'|(*:149)'
                        .'|/edit(*:162)'
                    .')'
                    .'|delete/([^/]++)(*:186)'
                .')'
                .'|/pres(?'
                    .'|entation/([^/]++)(?'
                        .'|(*:223)'
                        .'|/edit(*:236)'
                        .'|(*:244)'
                    .')'
                    .'|tations/([^/]++)(?'
                        .'|(*:272)'
                        .'|/edit(*:285)'
                        .'|(*:293)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        32 => [[['_route' => 'app_actualite_show', '_controller' => 'App\\Controller\\ActualiteController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        44 => [[['_route' => 'app_actualite_edit', '_controller' => 'App\\Controller\\ActualiteController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        67 => [[['_route' => 'app_actualite_delete', '_controller' => 'App\\Controller\\ActualiteController::delete'], ['id'], ['GET' => 0], null, false, true, null]],
        99 => [[['_route' => 'app_coordonnees_show', '_controller' => 'App\\Controller\\CoordonneesController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        111 => [[['_route' => 'app_coordonnees_edit', '_controller' => 'App\\Controller\\CoordonneesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        119 => [[['_route' => 'app_coordonnees_delete', '_controller' => 'App\\Controller\\CoordonneesController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        149 => [[['_route' => 'app_media_show', '_controller' => 'App\\Controller\\MediaController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        162 => [[['_route' => 'app_media_edit', '_controller' => 'App\\Controller\\MediaController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        186 => [[['_route' => 'app_media_delete', '_controller' => 'App\\Controller\\MediaController::delete'], ['id'], ['GET' => 0], null, false, true, null]],
        223 => [[['_route' => 'app_presentation_show', '_controller' => 'App\\Controller\\PresentationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        236 => [[['_route' => 'app_presentation_edit', '_controller' => 'App\\Controller\\PresentationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        244 => [[['_route' => 'app_presentation_delete', '_controller' => 'App\\Controller\\PresentationController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        272 => [[['_route' => 'app_prestations_show', '_controller' => 'App\\Controller\\PrestationsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        285 => [[['_route' => 'app_prestations_edit', '_controller' => 'App\\Controller\\PrestationsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        293 => [
            [['_route' => 'app_prestations_delete', '_controller' => 'App\\Controller\\PrestationsController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
