<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* actualite/index.html.twig */
class __TwigTemplate_53759eef70c15bcde3ed5fc536cfd78c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "actualite/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\tActualités

";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "\t<style>
\t\t.pagination {
\t\t\tdisplay: inline-block;
\t\t}

\t\t.pagination a {
\t\t\tcolor: #F9F617;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #000000;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.current {
\t\t\tcolor: #000000;
\t\t\tpadding: 8px 16px;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: #F9F617;
\t\t\ttransition: background-color 0.3s;
\t\t\tborder: 1px solid #ddd;
\t\t}

\t\t.pagination a.active {
\t\t\tbackground-color: #4CAF50;
\t\t\tcolor: white;
\t\t\tborder: 1px solid #4CAF50;
\t\t}

\t\t.pagination a:hover:not(.active) {
\t\t\tbackground-color: #F9F617;
\t\t\tcolor: black;
\t\t}
\t</style>
";
    }

    // line 45
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "
\t<main class=\"mainActu\">
\t\t<div class=\"news\">
\t\t\t<p class=\"titreActu\">
\t\t\t\tDernières actualités
\t\t\t</p>
\t\t</div>

\t\t";
        // line 54
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 55
            echo "\t\t\t<a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_new");
            echo "\">
\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t</a>
\t\t";
        }
        // line 59
        echo "
\t\t";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["actualites"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["actualite"]) {
            // line 61
            echo "\t\t\t<div class=\"containerNewArticle\">
\t\t\t\t<div class=\"border\"></div>
\t\t\t\t";
            // line 63
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 64
                echo "                    <a title=\"Supprimer\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["actualite"], "id", [], "any", false, false, false, 64)]), "html", null, true);
                echo "\"><i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i></a>
\t\t\t\t\t<a href=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_actualite_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["actualite"], "id", [], "any", false, false, false, 65)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t";
            }
            // line 69
            echo "\t\t\t\t<p class=\"titreActu2\">
\t\t\t\t\t";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "titreArt", [], "any", false, false, false, 70), "html", null, true);
            echo "
\t\t\t\t</p>
\t\t\t\t<p class=\"dateActu\">
\t\t\t\t\t<small>Publié le :
\t\t\t\t\t\t";
            // line 74
            ((twig_get_attribute($this->env, $this->source, $context["actualite"], "dateArt", [], "any", false, false, false, 74)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "dateArt", [], "any", false, false, false, 74), "Y-m-d"), "html", null, true))) : (print ("")));
            echo "
\t\t\t\t\t</small>
\t\t\t\t</p>
\t\t\t\t<img class=\"imgActu\" src=\"/uploads/";
            // line 77
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "imageActu", [], "any", false, false, false, 77), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "titreArt", [], "any", false, false, false, 77), "html", null, true);
            echo "\">
\t\t\t\t<p class=\"textActu\">
\t\t\t\t\t";
            // line 79
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["actualite"], "descriptionArt", [], "any", false, false, false, 79), "html", null, true);
            echo "
\t\t\t\t</p>
\t\t\t</div>
\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 83
            echo "\t\t\t<p>
\t\t\t\tAucune actualité, veuillez en créer de nouvelles
\t\t\t</p>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actualite'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "
\t\t<div class=\"navigation\">
\t\t\t";
        // line 89
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, ($context["actualites"] ?? null));
        echo "
\t\t</div>

\t</main>

";
    }

    public function getTemplateName()
    {
        return "actualite/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 89,  191 => 87,  182 => 83,  173 => 79,  166 => 77,  160 => 74,  153 => 70,  150 => 69,  143 => 65,  138 => 64,  136 => 63,  132 => 61,  127 => 60,  124 => 59,  116 => 55,  114 => 54,  104 => 46,  100 => 45,  62 => 9,  58 => 8,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "actualite/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/actualite/index.html.twig");
    }
}
