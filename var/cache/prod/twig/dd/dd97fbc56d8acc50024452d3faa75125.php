<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* media/index.html.twig */
class __TwigTemplate_084d7cdc9bab987385cf2c40858ddfcb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "media/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\tMedia
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t<main id=\"bodygalerie\">
\t\t<h2 class=\"h1\">
\t\t\tGalerie Photos et Vidéos
\t\t</h2>

\t\t";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 14
            echo "\t\t\t<a title=\"Nouveau\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_new");
            echo "\">
\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x fa-icon\" style=\"color:black\"></i>
\t\t\t</a>
\t\t";
        }
        // line 18
        echo "
\t\t<div class=\"mesphotos\">

\t\t\t";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["media"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["medium"]) {
            // line 22
            echo "
\t\t\t\t";
            // line 23
            if (twig_get_attribute($this->env, $this->source, $context["medium"], "image", [], "any", false, false, false, 23)) {
                // line 24
                echo "\t\t\t\t\t<img class=\"imagesbdd\" src=\"/uploads/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "image", [], "any", false, false, false, 24), "html", null, true);
                echo "\" alt=\"photofilm\">
\t\t\t\t";
            }
            // line 26
            echo "
\t\t\t\t";
            // line 27
            if (twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 27)) {
                // line 28
                echo "\t\t\t\t\t<video controls style=\"width: 25vw; height: 30vh\">
\t\t\t\t\t\t<source src=\"/uploads/";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 29), "html", null, true);
                echo "\" type=\"video/webm\">
\t\t\t\t\t\t<source src=\"/uploads/";
                // line 30
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["medium"], "video", [], "any", false, false, false, 30), "html", null, true);
                echo "\" type=\"video/mp4\">
\t\t\t\t\t\tSorry, your browser doesn't support embedded videos.
\t\t\t\t\t</video>
\t\t\t\t\t<a title=\"Supprimer\" href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t";
            }
            // line 37
            echo "\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 38
                echo "\t\t\t\t\t<a title=\"Supprimer\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 38)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-trash fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_media_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["medium"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t";
            }
            // line 45
            echo "\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 46
            echo "\t\t\t\t<p>
\t\t\t\t\tAucune photo(s) ou video(s) n'est disponible
\t\t\t\t</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['medium'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "\t\t</div>

\t\t<div class=\"separation\">
\t\t\t<img class=\"separe\" src=\"/images/lames.png\" alt=\"lames\">
\t\t</div>
\t\t<div id=\"mesvideos\">
\t\t\t";
        // line 56
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 57
            echo "\t\t\t\t<form action=\"/URL\" method=\"post\">
\t\t\t\t\t<label for=\"url\">URL</label>
\t\t\t\t\t<input name=\"url\" type=\"text\">
\t\t\t\t\t<button type=\"submit\">OK</button>
\t\t\t\t</form>
\t\t\t";
        }
        // line 63
        echo "        <iframe width=\"800\"height=\"600\" src=\"https://www.youtube.com/embed/";
        echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
        echo "\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t</div>
\t</main>

";
    }

    public function getTemplateName()
    {
        return "media/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 63,  164 => 57,  162 => 56,  154 => 50,  145 => 46,  140 => 45,  133 => 41,  126 => 38,  123 => 37,  116 => 33,  110 => 30,  106 => 29,  103 => 28,  101 => 27,  98 => 26,  92 => 24,  90 => 23,  87 => 22,  82 => 21,  77 => 18,  69 => 14,  67 => 13,  60 => 8,  56 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "media/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/media/index.html.twig");
    }
}
