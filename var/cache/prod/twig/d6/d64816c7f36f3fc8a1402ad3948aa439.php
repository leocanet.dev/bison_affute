<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* prestations/index.html.twig */
class __TwigTemplate_33c630b3f2b697a44da3369eb18e4746 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "prestations/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\tPrestations


";
    }

    // line 9
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "\t<link rel=\"stylesheet\" href=\"/styles/services.css\">
";
    }

    // line 13
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "
\t<main class=\"main_service\">
\t\t<div class=\"container_services\">

\t\t\t<div class=\"container_center\">
\t\t\t\t<img class=\"img_anim_service\" src=\"/images/lames.png\" alt=\"lames\">
\t\t\t</div>
\t\t\t<div class=\"container_right\">

\t\t\t\t<h2 class=\"gras\">
\t\t\t\t\tParticuliers & Professionnels
\t\t\t\t</h2>
\t\t\t\t<p>
\t\t\t\t\tJe vous accueille à mon atelier, pour effectuer des prestations d’affutâge.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de bouche:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tTraiteurs, bouchers, charcutiers, restaurateurs, poissonniers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tArtisans:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tCoiffeurs, couturiers, selliers, relieurs, cordonniers, tapissiers.


\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<span class=\"borderYellow_header\">
\t\t\t\t\t\tMétiers de la nature:


\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tÉleveurs, agriculteurs, jardiniers,paysagistes.


\t\t\t\t</p>
\t\t\t</div>
\t\t</div>
\t\t<h2 class=\"h2Service\">
\t\t\tTarifs Affûtage 2022
\t\t</h2>

\t\t<div class=\"main_prix\">
\t\t\t";
        // line 72
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 73
            echo "\t\t\t\t<a title=\"Nouveau\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_new");
            echo "\">
\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t\t</a>
\t\t\t";
        }
        // line 77
        echo "\t\t\t<div class=\"container_Prix_Head2\">
\t\t\t\t<div class=\"container_prix\">
\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRESTATIONS


\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p class=\"gras\">
\t\t\t\t\t\t\tPRIX


\t\t\t\t\t\t</p>
\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 92
        $context["brands"] = [];
        // line 93
        echo "\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["prestations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["prestation"]) {
            // line 94
            echo "\t\t\t\t\t\t";
            if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 94), ($context["brands"] ?? null))) {
                // line 95
                echo "\t\t\t\t\t\t\t";
                $context["brands"] = twig_array_merge(($context["brands"] ?? null), [0 => twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 95)]);
                // line 96
                echo "\t\t\t\t\t\t\t<p class=\"titre_prestation\">
\t\t\t\t\t\t\t\t";
                // line 97
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "rubrique", [], "any", false, false, false, 97), "html", null, true);
                echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
            }
            // line 100
            echo "\t\t\t\t\t\t<div class=\"container_row\">
\t\t\t\t\t\t\t<p style=\"word-break: break-all; width: 65%;\">
\t\t\t\t\t\t\t\t";
            // line 102
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "prestation", [], "any", false, false, false, 102), "html", null, true);
            echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t";
            // line 104
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 105
                echo "\t\t\t\t\t\t\t\t<a title=\"Modifier\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prestations_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["prestation"], "id", [], "any", false, false, false, 105)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:yellow\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
            }
            // line 109
            echo "\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
            // line 110
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prestation"], "prix", [], "any", false, false, false, 110), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t€


\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prestation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</main>

";
    }

    public function getTemplateName()
    {
        return "prestations/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 117,  203 => 110,  200 => 109,  192 => 105,  190 => 104,  185 => 102,  181 => 100,  175 => 97,  172 => 96,  169 => 95,  166 => 94,  161 => 93,  159 => 92,  142 => 77,  134 => 73,  132 => 72,  72 => 14,  68 => 13,  63 => 10,  59 => 9,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "prestations/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/prestations/index.html.twig");
    }
}
