<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_1edf1e9eae4f9c521418d985c121fb1b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t\t<link rel=\"icon\" href=\"/images/couteau2.png\">
\t\t<link rel=\"stylesheet\" href=\"/styles/style.css\">
\t\t<meta name=\"description\" content=\"Monsieur Marc Avérous,
\t\t\t                            affûteur rémouleur situé dans le département de l'Aude plus précisément sur la commune de Saint-Nazaire-d'Aude.
\t\t\t                            Propose un service de Rémoulage Affûtage pour vos vos couteaux, ciseaux, ciseaux à bois, sécateurs, cisailles haies,
\t\t\t                            taille-haies thermiques ou électriques, les lames de coupe jambon, les chaînes de tronçonneuses,
\t\t\t                            tous les outils de jardin.
\t\t\t                            \">
\t\t<title>
\t\t\t";
        // line 16
        $this->displayBlock('title', $context, $blocks);
        // line 19
        echo "\t\t</title>

\t</head>
\t<body>
\t\t";
        // line 23
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 24
        echo "\t\t";
        $this->displayBlock('header', $context, $blocks);
        // line 31
        echo "\t\t";
        $this->displayBlock('body', $context, $blocks);
        // line 32
        echo "\t\t";
        $this->displayBlock('footer', $context, $blocks);
        // line 35
        echo "
\t</body>
</html>
<script src=\"https://kit.fontawesome.com/169ff619a2.js\" crossorigin=\"anonymous\"></script>
";
    }

    // line 16
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "\t\t\t\tBison affûté
\t\t\t";
    }

    // line 23
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 24
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "\t\t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 26
            echo "\t\t\t\t";
            $this->loadTemplate("/header/header_admin.html.twig", "base.html.twig", 26)->display($context);
            // line 27
            echo "\t\t\t";
        } else {
            // line 28
            echo "\t\t\t\t";
            $this->loadTemplate("/header/header.html.twig", "base.html.twig", 28)->display($context);
            // line 29
            echo "\t\t\t";
        }
        // line 30
        echo "\t\t";
    }

    // line 31
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 32
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "\t\t\t";
        $this->loadTemplate("/footer/footer.html.twig", "base.html.twig", 33)->display($context);
        // line 34
        echo "\t\t";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  137 => 34,  134 => 33,  130 => 32,  124 => 31,  120 => 30,  117 => 29,  114 => 28,  111 => 27,  108 => 26,  105 => 25,  101 => 24,  95 => 23,  90 => 17,  86 => 16,  78 => 35,  75 => 32,  72 => 31,  69 => 24,  67 => 23,  61 => 19,  59 => 16,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/base.html.twig");
    }
}
