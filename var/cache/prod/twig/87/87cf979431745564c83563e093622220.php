<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* presentation/index.html.twig */
class __TwigTemplate_b793e229928a2a223ffdf8b6ef74bf94 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "presentation/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Présentation
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
\t<div id=\"bodyProposDiv\">
\t\t<main class=\"mainPropos\">

\t\t\t";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["presentations"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["presentation"]) {
            // line 13
            echo "\t\t\t\t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 14
                echo "\t\t\t\t\t<a title=\"Nouveau\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_new");
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a title=\"Modifier\" href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["presentation"], "id", [], "any", false, false, false, 17)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t<i class=\"fa-solid fa-file-pen fa-2x\" style=\"color:black\"></i>
\t\t\t\t\t</a>
\t\t\t\t";
            }
            // line 21
            echo "\t\t\t\t<h2 class=\"h1Apropos\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "titrePres", [], "any", false, false, false, 21), "html", null, true);
            echo "</h2>

\t\t\t\t<p class=\"textPropos\">";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe1Pres", [], "any", false, false, false, 23), "html", null, true);
            echo "</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "ImagePres1", [], "any", false, false, false, 24), "html", null, true);
            echo "\" alt=\"Premiere image\"/>
\t\t\t\t<p class=\"textPropos\">";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe2Pres", [], "any", false, false, false, 25), "html", null, true);
            echo "</p>
\t\t\t\t<img class=\"imgPropos\" src=\"/uploads/";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "ImagePres2", [], "any", false, false, false, 26), "html", null, true);
            echo "\" alt=\"Deuxieme image\"/>
\t\t\t\t<p class=\"textPropos\">";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["presentation"], "paragraphe3Pres", [], "any", false, false, false, 27), "html", null, true);
            echo "</p>

\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 30
            echo "            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 31
                echo "                <a title=\"Nouveau\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_presentation_new");
                echo "\">
                    <i class=\"fa-solid fa-file-circle-plus fa-2x\" style=\"color:black\"></i>
                </a>
            ";
            }
            // line 35
            echo "\t\t\t\t<p>Aucune description, veuillez créer une description</p>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['presentation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t\t</main>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "presentation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 37,  128 => 35,  120 => 31,  117 => 30,  109 => 27,  105 => 26,  101 => 25,  97 => 24,  93 => 23,  87 => 21,  80 => 17,  73 => 14,  70 => 13,  65 => 12,  59 => 8,  55 => 7,  47 => 4,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "presentation/index.html.twig", "/Users/leo.canet/Desktop/Bison_affuté_Sym/bison_affuté/templates/presentation/index.html.twig");
    }
}
