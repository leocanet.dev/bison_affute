<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerKbHjxni\App_KernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerKbHjxni/App_KernelProdContainer.php') {
    touch(__DIR__.'/ContainerKbHjxni.legacy');

    return;
}

if (!\class_exists(App_KernelProdContainer::class, false)) {
    \class_alias(\ContainerKbHjxni\App_KernelProdContainer::class, App_KernelProdContainer::class, false);
}

return new \ContainerKbHjxni\App_KernelProdContainer([
    'container.build_hash' => 'KbHjxni',
    'container.build_id' => 'd2e8193e',
    'container.build_time' => 1647616238,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerKbHjxni');
