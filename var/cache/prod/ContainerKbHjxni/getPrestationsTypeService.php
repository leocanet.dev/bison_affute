<?php

namespace ContainerKbHjxni;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getPrestationsTypeService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Form\PrestationsType' shared autowired service.
     *
     * @return \App\Form\PrestationsType
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Form\\PrestationsType'] = new \App\Form\PrestationsType();
    }
}
