<?php

namespace ContainerKbHjxni;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getCoordonneesControllerService extends App_KernelProdContainer
{
    /*
     * Gets the public 'App\Controller\CoordonneesController' shared autowired service.
     *
     * @return \App\Controller\CoordonneesController
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->services['App\\Controller\\CoordonneesController'] = $instance = new \App\Controller\CoordonneesController();

        $instance->setContainer(($container->privates['.service_locator.jIxfAsi'] ?? $container->load('get_ServiceLocator_JIxfAsiService'))->withContext('App\\Controller\\CoordonneesController', $container));

        return $instance;
    }
}
