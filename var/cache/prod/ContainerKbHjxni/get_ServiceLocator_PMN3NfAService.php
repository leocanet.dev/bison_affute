<?php

namespace ContainerKbHjxni;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_PMN3NfAService extends App_KernelProdContainer
{
    /*
     * Gets the private '.service_locator.pMN3NfA' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.pMN3NfA'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'VideoYouTubeRepository' => ['privates', 'App\\Repository\\VideoYouTubeRepository', 'getVideoYouTubeRepositoryService', true],
            'mediaRepository' => ['privates', 'App\\Repository\\MediaRepository', 'getMediaRepositoryService', true],
        ], [
            'VideoYouTubeRepository' => 'App\\Repository\\VideoYouTubeRepository',
            'mediaRepository' => 'App\\Repository\\MediaRepository',
        ]);
    }
}
