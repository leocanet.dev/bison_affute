<?php

namespace ContainerKbHjxni;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getPresentationService extends App_KernelProdContainer
{
    /*
     * Gets the private '.errored..service_locator.ESx.8CO.App\Entity\Presentation' shared service.
     *
     * @return \App\Entity\Presentation
     */
    public static function do($container, $lazyLoad = true)
    {
        throw new RuntimeException('Cannot autowire service ".service_locator.ESx.8CO": it references class "App\\Entity\\Presentation" but no such service exists.');
    }
}
