<?php

namespace App\Form;

use App\Entity\Presentation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\File;

class PresentationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre_pres', TextType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '2',
                    'max' => '100'
                ],
                'label' => 'Titre',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer un titre",
                    ])
                ]
            ])
            ->add('paragraphe1_pres', TextareaType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '10',
                    'max' => '1500'
                ],
                'label' => 'Paragraphe n°1',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 1500]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer une description",
                    ])
                ]
            ])
            ->add('image_pres_1', FileType::class, [
                'mapped' => false,
                'required' => false, 'constraints' => [
                    new File([
                        'mimeTypes' => ['image/*']
                    ])
                ]
            ])
            ->add('paragraphe2_pres', TextareaType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '10',
                    'max' => '1500'
                ],
                'label' => 'Paragraphe n°2',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 1500]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer une description",
                    ])
                ]
            ])
            ->add('image_pres_2', FileType::class, [
                'mapped' => false,
                'required' => false, 'constraints' => [
                    new File([
                        'mimeTypes' => ['image/*']
                    ])
                ]
            ])
            ->add('paragraphe3_pres', TextareaType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '10',
                    'max' => '1500'
                ],
                'label' => 'Paragraphe n°3',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 1500]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer une description",
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Presentation::class,
        ]);
    }
}
