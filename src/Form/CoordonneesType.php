<?php

namespace App\Form;

use App\Entity\Coordonnees;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CoordonneesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('adresse', TextType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '5',
                    'max' => '255'
                ],
                'label' => 'Adresse',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 5, 'max' => 255]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer votre adresse",
                    ])
                ]
            ])
            ->add('telephone', TelType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '10',
                    'max' => '15'
                ],
                'label' => 'Téléphone',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 15]),
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ])
            ->add('mail', EmailType::class, [
                'attr' => [
                    'class' => 'imput-form',
                    'min' => '2',
                    'max' => '100'
                ],
                'label' => 'Adresse e-mail',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100]),
                    new Assert\NotBlank([
                        'message' => "Veuillez renseigner votre adresse e-mail",
                    ])
                ]
            ])
            ->add('horaires', TextType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '5',
                    'max' => '255'
                ],
                'label' => 'Horaires',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 5, 'max' => 255]),
                    new Assert\NotBlank([
                        'message' => "Veuillez renseigner vos horaires",
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Coordonnees::class,
        ]);
    }
}
