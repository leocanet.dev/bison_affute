<?php

namespace App\Form;

use App\Entity\Media;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Image', 'required' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => ['image/*']
                    ])
                ]
            ])
            ->add('video', FileType::class, [
                'mapped' => false,
                'label' => 'Video', 'required' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => ['video/*']
                    ])
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Media::class,
        ]);
    }
}
