<?php

namespace App\Form;

use App\Entity\Actualite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\File;

class ActualiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre_art', TextType::class, [
                'attr' => [
                    'class' => 'title-actu',
                    'min' => '3',
                    'max' => '255'
                ],
                'label' => 'Titre',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 3, 'max' => 255]),
                    new Assert\NotBlank([
                        'message' => "Veuillez renseigner le titre de l'article",
                    ])
                ]
            ])
            ->add('image_actu', FileType::class, [
                'mapped'=> false,
                'attr' => [
                    'class' => 'img-actu',
                ],
                'label' => 'Image', 'required' => false,
                'label_attr' => [
                    'class' => 'label'
                ], 'constraints' => [
                    new File([
                        'mimeTypes' => ['image/*']
                    ])
                ]
            ])
            ->add('description_art', TextareaType::class, [
                'attr' => [
                    'class' => 'descr-actu',
                    'min' => '10',
                    'max' => '1500'
                ],
                'label' => 'Description',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 1500]),
                    new Assert\NotBlank()
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Actualite::class,
        ]);
    }
}
