<?php

namespace App\Form;

use App\Entity\Prestations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PrestationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rubrique', TextType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '3',
                    'max' => '30'
                ],
                'label' => 'Rubrique',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 3, 'max' => 50]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer une rubrique",
                    ])
                ]
            ])
            ->add('prestation', TextareaType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '4',
                    'max' => '1500'
                ],
                'label' => 'Prestation',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 4, 'max' => 1500]),
                    new Assert\NotBlank([
                        'message' => "Veuillez entrer une prestation",
                    ])
                ]
            ])
            ->add('prix', NumberType::class, [
                'attr' => [
                    'class' => 'input-form',
                    'min' => '1',
                    'max' => '10000'
                ],
                'label' => 'Prix',
                'label_attr' => [
                    'class' => 'label'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 1, 'max' => 10000]),
                    new Assert\NotBlank([
                        'message' => "Veuillez saisir un nombre",
                    ]),
                    new Assert\Positive()
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Prestations::class,
        ]);
    }
}
