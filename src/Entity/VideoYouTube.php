<?php

namespace App\Entity;

use App\Repository\VideoYouTubeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VideoYouTubeRepository::class)]
class VideoYouTube
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text', nullable: true)]
    private $UrlVideo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrlVideo(): ?string
    {
        return $this->UrlVideo;
    }

    public function setUrlVideo(?string $UrlVideo): self
    {
        $this->UrlVideo = $UrlVideo;

        return $this;
    }
}
