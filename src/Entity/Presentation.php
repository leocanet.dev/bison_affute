<?php

namespace App\Entity;

use App\Repository\PresentationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PresentationRepository::class)]
class Presentation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $titre_pres;

    #[ORM\Column(type: 'text', nullable: true)]
    private $paragraphe1_pres;

    #[ORM\Column(type: 'text', nullable: true)]
    private $paragraphe2_pres;

    #[ORM\Column(type: 'text', nullable: true)]
    private $paragraphe3_pres;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image_pres_1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $image_pres_2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitrePres(): ?string
    {
        return $this->titre_pres;
    }

    public function setTitrePres(?string $titre_pres): self
    {
        $this->titre_pres = $titre_pres;

        return $this;
    }

    public function getParagraphe1Pres(): ?string
    {
        return $this->paragraphe1_pres;
    }

    public function setParagraphe1Pres(?string $paragraphe1_pres): self
    {
        $this->paragraphe1_pres = $paragraphe1_pres;

        return $this;
    }

    public function getParagraphe2Pres(): ?string
    {
        return $this->paragraphe2_pres;
    }

    public function setParagraphe2Pres(?string $paragraphe2_pres): self
    {
        $this->paragraphe2_pres = $paragraphe2_pres;

        return $this;
    }

    public function getParagraphe3Pres(): ?string
    {
        return $this->paragraphe3_pres;
    }

    public function setParagraphe3Pres(?string $paragraphe3_pres): self
    {
        $this->paragraphe3_pres = $paragraphe3_pres;

        return $this;
    }

    public function getImagePres1(): ?string
    {
        return $this->image_pres_1;
    }

    public function setImagePres1(?string $image_pres_1): self
    {
        $this->image_pres_1 = $image_pres_1;

        return $this;
    }

    public function getImagePres2(): ?string
    {
        return $this->image_pres_2;
    }

    public function setImagePres2(?string $image_pres_2): self
    {
        $this->image_pres_2 = $image_pres_2;

        return $this;
    }
}
