<?php

namespace App\Controller;

use App\Entity\Prestations;
use App\Form\PrestationsType;
use App\Repository\PrestationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/prestations')]
class PrestationsController extends AbstractController
{
    #[Route('/', name: 'app_prestations_index', methods: ['GET'])]
    public function index(PrestationsRepository $prestationsRepository): Response
    {
        return $this->render('prestations/index.html.twig', [
            'prestations' => $prestationsRepository->getByRubriques(),
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/new', name: 'app_prestations_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PrestationsRepository $prestationsRepository): Response
    {
        $prestation = new Prestations();
        $form = $this->createForm(PrestationsType::class, $prestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $prestationsRepository->add($prestation);
            return $this->redirectToRoute('app_prestations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('prestations/new.html.twig', [
            'prestation' => $prestation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_prestations_show', methods: ['GET'])]
    public function show(Prestations $prestation): Response
    {
        return $this->render('prestations/show.html.twig', [
            'prestation' => $prestation,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/{id}/edit', name: 'app_prestations_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Prestations $prestation, PrestationsRepository $prestationsRepository): Response
    {
        $form = $this->createForm(PrestationsType::class, $prestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $prestationsRepository->add($prestation);
            return $this->redirectToRoute('app_prestations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('prestations/edit.html.twig', [
            'prestation' => $prestation,
            'form' => $form,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/{id}', name: 'app_prestations_delete', methods: ['POST'])]
    public function delete(Request $request, Prestations $prestation, PrestationsRepository $prestationsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$prestation->getId(), $request->request->get('_token'))) {
            $prestationsRepository->remove($prestation);
        }

        return $this->redirectToRoute('app_prestations_index', [], Response::HTTP_SEE_OTHER);
    }
}
