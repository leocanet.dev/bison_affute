<?php

namespace App\Controller;

use App\Entity\Coordonnees;
use App\Form\CoordonneesType;
use App\Repository\CoordonneesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/coordonnees')]
class CoordonneesController extends AbstractController
{
    #[Route('/', name: 'app_coordonnees_index', methods: ['GET'])]
    public function index(CoordonneesRepository $coordonneesRepository): Response
    {
        return $this->render('coordonnees/index.html.twig', [
            'coordonnees' => $coordonneesRepository->findAll(),
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/new', name: 'app_coordonnees_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CoordonneesRepository $coordonneesRepository): Response
    {
        $coordonnee = new Coordonnees();
        $form = $this->createForm(CoordonneesType::class, $coordonnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $coordonneesRepository->add($coordonnee);
            return $this->redirectToRoute('app_coordonnees_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coordonnees/new.html.twig', [
            'coordonnee' => $coordonnee,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_coordonnees_show', methods: ['GET'])]
    public function show(Coordonnees $coordonnee): Response
    {
        return $this->render('coordonnees/show.html.twig', [
            'coordonnee' => $coordonnee,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/{id}/edit', name: 'app_coordonnees_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Coordonnees $coordonnee, CoordonneesRepository $coordonneesRepository): Response
    {
        $form = $this->createForm(CoordonneesType::class, $coordonnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $coordonneesRepository->add($coordonnee);
            return $this->redirectToRoute('app_coordonnees_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coordonnees/edit.html.twig', [
            'coordonnee' => $coordonnee,
            'form' => $form,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/{id}', name: 'app_coordonnees_delete', methods: ['POST'])]
    public function delete(Request $request, Coordonnees $coordonnee, CoordonneesRepository $coordonneesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$coordonnee->getId(), $request->request->get('_token'))) {
            $coordonneesRepository->remove($coordonnee);
        }

        return $this->redirectToRoute('app_coordonnees_index', [], Response::HTTP_SEE_OTHER);
    }
}
