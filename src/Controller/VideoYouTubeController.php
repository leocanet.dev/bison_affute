<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\VideoYouTube;
use App\Repository\VideoYouTubeRepository;
use Symfony\Component\HttpFoundation\Request;

class VideoYouTubeController extends AbstractController
{
    #[Route('/URL', name: 'app_VideoYouTube', methods: ['POST'])]
    public function index(Request $request,VideoYouTubeRepository $VideoYouTubeRepository){
        $url = $VideoYouTubeRepository->findAll();
        if(!$url){
            $url = new VideoYouTube();
        }else{
            $url = $url[0];
        }

        $urlPageYT = $request->request->get('url');
        $idYT = explode("?v=", $urlPageYT)[1];
        $url->setUrlVideo($idYT);
        $VideoYouTubeRepository->add($url);
        

        return $this->redirectToRoute('app_media_index', [], Response::HTTP_SEE_OTHER);
   
    }
}