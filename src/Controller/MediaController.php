<?php

namespace App\Controller;

use App\Entity\Media;
use App\Form\MediaType;
use App\Repository\MediaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Repository\VideoYouTubeRepository;

#[Route('/media')]
class MediaController extends AbstractController
{
    #[Route('/', name: 'app_media_index', methods: ['GET'])]
    public function index(VideoYouTubeRepository $VideoYouTubeRepository, MediaRepository $mediaRepository): Response
    {
        return $this->render('media/index.html.twig', [
            'media' => $mediaRepository->findAll(),
            'url'=>  $VideoYouTubeRepository->findAll()[0]->getUrlVideo()
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/new', name: 'app_media_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FileUploader $fileUploader, MediaRepository $mediaRepository): Response
    {
        //Image et video upload
        $medium = new Media();
        $form = $this->createForm(MediaType::class, $medium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFile = $form->get('image')->getData();
            $videoFile = $form->get('video')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);

                $medium->setImage($imageFileName);
            }

            if ($videoFile) {
                $videoFileName = $fileUploader->upload($videoFile);

                $medium->setVideo($videoFileName);
            }

            $mediaRepository->add($medium);
            return $this->redirectToRoute('app_media_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('media/new.html.twig', [
            'medium' => $medium,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_media_show', methods: ['GET'])]
    public function show(Media $medium): Response
    {
        return $this->render('media/show.html.twig', [
            'medium' => $medium,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/{id}/edit', name: 'app_media_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, FileUploader $fileUploader, Media $medium, MediaRepository $mediaRepository): Response
    {
        $form = $this->createForm(MediaType::class, $medium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('image')->getData();
            $videoFile = $form->get('video')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
    
                $medium->setImage($imageFileName);
            }
    
            if ($videoFile) {
                $videoFileName = $fileUploader->upload($videoFile);
    
                $medium->setVideo($videoFileName);
            }

            $mediaRepository->add($medium);
            return $this->redirectToRoute('app_media_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('media/edit.html.twig', [
            'medium' => $medium,
            'form' => $form,
        ]);
    }
    #[Security("is_granted('ROLE_ADMIN')")]
    #[Route('/delete/{id}', name: 'app_media_delete', methods: ['GET'])]
    public function delete(Request $request, Media $medium, MediaRepository $mediaRepository): Response
    {
        // if ($this->isCsrfTokenValid('delete'.$medium->getId(), $request->request->get('_token'))) {
            $mediaRepository->remove($medium);
        //}

        return $this->redirectToRoute('app_media_index', [], Response::HTTP_SEE_OTHER);
    }
}
